package com.maciek.CleanApp.mappers;

import com.maciek.CleanApp.dto.EmployeeDto;
import com.maciek.CleanApp.model.Employee;
import com.maciek.CleanApp.model.Mark;

import java.util.List;
import java.util.stream.Collectors;

public class EmployeeToEmployeeDtoMapper {


    public static EmployeeDto mapperEmployeeToEmployeeDto(Employee employee) {

        EmployeeDto employeeDto = new EmployeeDto();


        employeeDto.setId(employee.getId());
        employeeDto.setLogin(employee.getLogin());
        employeeDto.setPassword(employee.getPassword());
        employeeDto.setName(employee.getName());
        employeeDto.setSurname(employee.getSurname());
        employeeDto.setPhoneNo(employee.getPhoneNo());
        employeeDto.setMarks(employee.getMarks());
        employeeDto.setCalendar(employee.getCalendar());
        employeeDto.setOffers(employee.getOffers());
        Double averageScore = getAverageScore(employee.getMarks());
        employeeDto.setScoreAverage(averageScore);

        return employeeDto;
    }

    public static List<EmployeeDto> mapEmployeesToEmployeeDtos(List<Employee> employees) {
        List<EmployeeDto> employerDtoList = employees.stream().map(e -> mapperEmployeeToEmployeeDto(e)).collect(Collectors.toList());

        return employerDtoList;
    }

    private static Double getAverageScore(List<Mark> marks) {

        return marks.stream()
                .collect(Collectors.averagingInt(m -> m.getScore()));
    }

}
