package com.maciek.CleanApp.mappers;

import com.maciek.CleanApp.dto.EmployerDto;
import com.maciek.CleanApp.model.Employer;
import com.maciek.CleanApp.model.Mark;
import com.maciek.CleanApp.services.MarkService;

import java.util.List;
import java.util.stream.Collectors;

public class EmployerToEmployerDtoMapper {


    public static EmployerDto mapperEmployerToEmployerDto(Employer employer) {

        EmployerDto employerDto = new EmployerDto();

        employerDto.setId(employer.getId());
        employerDto.setLogin(employer.getLogin());
        employerDto.setPassword(employer.getPassword());
        employerDto.setName(employer.getName());
        employerDto.setSurname(employer.getSurname());
        employerDto.setPhoneNo(employer.getPhoneNo());
        employerDto.setApartments(employer.getApartments());
        employerDto.setMarks(employer.getMarks());
        employerDto.setChallenges(employer.getChallenges());
        Double averageScore = getAverageScore(employer.getMarks());
        employerDto.setScoreAverage(averageScore);
        return employerDto;
    }

    public static List<EmployerDto> mapEmployersToEmployersDtos(List<Employer> employers) {

        List<EmployerDto> employerDtoList = employers.stream().map(e -> mapperEmployerToEmployerDto(e)).collect(Collectors.toList());

        return employerDtoList;
    }


    private static Double getAverageScore(List<Mark> marks) {

        return marks.stream()
                .collect(Collectors.averagingInt(m -> m.getScore()));
    }

}
