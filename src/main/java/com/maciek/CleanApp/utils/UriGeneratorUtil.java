package com.maciek.CleanApp.utils;

import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.net.URI;

public class UriGeneratorUtil {

    public static  URI generatePathForId(Object id) {

        return MvcUriComponentsBuilder.fromController(id.getClass())
                .pathSegment(id.toString())
                .build()
                .toUri();

    }
}
