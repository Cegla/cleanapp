package com.maciek.CleanApp.exceptions;

public class LoginExistsException extends RuntimeException {


    public LoginExistsException(String login) {
        super(String.format("Login : %s already exists", login));
    }
}
