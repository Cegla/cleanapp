package com.maciek.CleanApp.model;


import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "appointments")
public class Appointment {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Long id;

	@Column(name = "start_date")
	private LocalDateTime startDate;


    @Column(name = "end_date")
    private LocalDateTime endDate;

	private Integer rate;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private AppointmentStatus status;

    public Appointment() {

    }



    public Appointment(LocalDateTime startDate, LocalDateTime endDate, Integer rate, AppointmentStatus status) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.rate = rate;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public AppointmentStatus getStatus() {
        return status;
    }

    public void setStatus(AppointmentStatus status) {
        this.status = status;
    }


    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }


    @Override
    public String toString() {
        return "Appointment{" +
                "id=" + id +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", rate=" + rate +
                ", status=" + status +
                '}';
    }


}
