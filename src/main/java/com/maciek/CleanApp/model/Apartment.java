package com.maciek.CleanApp.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "apartments")
public class Apartment {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank
    @Size(min = 3, message = "Apartment name can not have less than two chars")
    private String name;

    @Min(value = 1, message = "Room no can not be less than 1")
    private Integer roomNumbers;

    @NotBlank
    @Size(min = 2, message = "Apartment name can not have less than two chars")
    private String street;

    @NotBlank
    @Size(min = 2, message = "Apartment name can not have less than two chars")
    private String city;

    @ManyToOne
    @JoinColumn(name = "employer_id")
    @JsonIgnore
    private Employer employer;
    

    @OneToOne
    @JoinColumn(name = "calendar_id")
    private Calendar calendar;

    @OneToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    private List<Challenge> challenges = new ArrayList<>();



    public Apartment() {
    }

    public Apartment(String name, Integer roomNumbers, String street, String city, Employer employer, Calendar calendar, List<Challenge> challenges) {
        this.name = name;
        this.roomNumbers = roomNumbers;
        this.street = street;
        this.city = city;
        this.employer = employer;
        this.calendar = calendar;
        this.challenges = challenges;
    }

    public Apartment(String name, Integer roomNumbers, String street, String city, Employer employer) {
        this.name = name;
        this.roomNumbers = roomNumbers;
        this.street = street;
        this.city = city;
        this.employer = employer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRoomNumbers() {
        return roomNumbers;
    }

    public void setRoomNumbers(Integer roomNumbers) {
        this.roomNumbers = roomNumbers;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }



    public Calendar getCalendar() {
        return calendar;
    }

    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }

    public List<Challenge> getChallenges() {
        return challenges;
    }

    public void setChallenges(List<Challenge> challenges) {
        this.challenges = challenges;
    }

    @Override
    public String toString() {
        return "Apartment{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", roomNumbers=" + roomNumbers +
                ", street='" + street + '\'' +
                ", city='" + city + '\'' +
                ", employer=" + employer +
                ", calendar=" + calendar +
                ", challenges=" + challenges +
                '}';
    }

}
