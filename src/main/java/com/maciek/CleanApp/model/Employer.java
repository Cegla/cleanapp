package com.maciek.CleanApp.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "employer")
public class Employer extends Client {


    @OneToMany(
            cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Apartment> apartments = new ArrayList<>();


    @OneToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    @JsonIgnore
    private List<Mark> marks = new ArrayList<>();

    @OneToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    private List<Challenge> challenges;


    public Employer() {
    }


    public Employer(List<Apartment> apartments, List<Mark> marks) {
        this.apartments = apartments;
        this.marks = marks;
    }


    public Employer(String login, String password, String name, String surname, String phoneNo, List<Apartment> apartments, List<Mark> marks) {
        super(login, password, name, surname, phoneNo);
        this.apartments = apartments;
        this.marks = marks;
    }

    public Employer(List<Challenge> challenges) {
        this.challenges = challenges;
    }

    public List<Apartment> getApartments() {
        return apartments;
    }

    public void setApartments(List<Apartment> apartments) {
        this.apartments = apartments;
    }

    public List<Mark> getMarks() {
        return marks;
    }

    public void setMarks(List<Mark> marks) {
        this.marks = marks;
    }

    public List<Challenge> getChallenges() {
        return challenges;
    }

    public void setChallenges(List<Challenge> challenges) {
        this.challenges = challenges;
    }

    @Override
    public String toString() {
        return "Employer{" +
                "apartments=" + apartments +
                ", marks=" + marks +
                ", challenges=" + challenges +
                '}';
    }


}
