package com.maciek.CleanApp.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    @NotBlank
    @Length(min = 4, max = 20, message = "Minimum length of login is 5 chars")
    private String login;

    @NotBlank
    @Column(length = 60)
    @Length(min = 6,message = "Minimum length of password is 6 chars")
    private String password;

    @NotBlank
    @Size(min = 2, message = "Name can not have less than two chars")
    protected String name;

    @NotBlank
    @Size(min = 2, message = "Surname can not have less than two chars")
    protected String surname;

    @NotBlank
    @Size(min = 9, message = "Phone number can not have less than nine digits")
    protected String phoneNo;

    public Client(String login, String password, String name, String surname, String phoneNo) {
        this.login = login;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.phoneNo = phoneNo;
    }

    public Client(){}

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", phoneNo='" + phoneNo + '\'' +
                '}';
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

}
