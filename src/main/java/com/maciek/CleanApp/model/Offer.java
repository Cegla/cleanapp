package com.maciek.CleanApp.model;


import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Entity
public class Offer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Min(value = 1, message = "Rate can not be less than 1")
    @Max(value = 10000, message = "Rate can not be higher than 10 000")
    private Integer rate;

    @Min(value = 1, message = "Timespan can not be shorter than 1 hour")
    private Integer duration;


    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;

    @ManyToOne
    @JoinColumn(name = "challenge_id")
    private Challenge challenge;



    public Offer() {
    }


    public Offer(Integer rate, Employee employee, Challenge challenge) {
        this.rate = rate;
        this.employee = employee;
        this.challenge = challenge;
    }

    public Offer(Integer rate, Integer duration, Employee employee, Challenge challenge) {
        this.rate = rate;
        this.duration = duration;
        this.employee = employee;
        this.challenge = challenge;
    }


    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Challenge getChallenge() {
        return challenge;
    }

    public void setChallenge(Challenge challenge) {
        this.challenge = challenge;
    }


    @Override
    public String toString() {
        return "Offer{" +
                "id=" + id +
                ", rate=" + rate +
                ", duration=" + duration +
                ", employee=" + employee +
                ", challenge=" + challenge +
                '}';
    }
}
