package com.maciek.CleanApp.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.maciek.CleanApp.deserializers.DataDeserializer;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Challenge {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull(message = "Date can not be blank")
    @DateTimeFormat(style = "yyyy-MM-dd HH:mm" )
    @JsonDeserialize(using = DataDeserializer.class)
    private LocalDateTime date;


    private Boolean status;

    @ManyToOne
    @JoinColumn(name = "apartments_id")
    @JsonIgnore
    private Apartment apartment = new Apartment();

    @OneToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    @JsonIgnore
    private List<Offer> offers = new ArrayList<>();


    public Challenge(){}

    public Challenge(LocalDateTime date, Boolean status, Apartment apartment, List<Offer> offers) {
        this.date = date;
        this.status = status;
        this.apartment = apartment;
        this.offers = offers;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Apartment getApartment() {
        return apartment;
    }

    public void setApartment(Apartment apartment) {
        this.apartment = apartment;
    }

    public List<Offer> getOffers() {
        return offers;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

    @Override
    public String toString() {
        return "Challenge{" +
                "id=" + id +
                ", date=" + date +
                ", status=" + status +
                ", apartment=" + apartment +
                ", offers=" + offers +
                '}';
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
