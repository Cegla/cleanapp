package com.maciek.CleanApp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "Employee")
public class Employee extends Client {


    @OneToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    @JsonIgnore
    private List<Mark> marks = new ArrayList<>();

    @OneToMany(
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    @JsonIgnore
    private List<Offer> offers = new ArrayList<>();


    @OneToOne
    @JoinColumn(name = "calendar_id")
    @JsonIgnore
    private Calendar calendar;


    public Employee() {

    }

    public Employee(String login, String password, String name, String surname, String phoneNo) {
        super(login, password, name, surname, phoneNo);
    }

    public Employee(String login, String password, String name, String surname, String phoneNo, Calendar calendar, List<Mark> marks) {
        super(login, password, name, surname, phoneNo);
        this.calendar = calendar;
        this.marks = marks;
    }

    public Employee(Calendar calendar, List<Mark> marks) {
        this.calendar = calendar;
        this.marks = marks;
    }


    public Calendar getCalendar() {
        return calendar;
    }

    public void setCalendar(Calendar calendar) {
        this.calendar = calendar;
    }

    public List<Mark> getMarks() {
        return marks;
    }

    public void setMarks(List<Mark> marks) {
        this.marks = marks;
    }

    public List<Offer> getOffers() {
        return offers;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "calendar=" + calendar +
                ", marks=" + marks +
                ", offers=" + offers +
                '}';
    }

}



