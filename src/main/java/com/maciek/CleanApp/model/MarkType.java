package com.maciek.CleanApp.model;


public enum MarkType {

    POOR(1), NOTHINGSPECIAL(2), GREAT(3);

    private double score;


    MarkType(double score) {
        this.score = score;
    }
}
