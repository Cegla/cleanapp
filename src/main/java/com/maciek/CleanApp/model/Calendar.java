package com.maciek.CleanApp.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "calendar")
public class Calendar {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;


    @OneToMany(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL

    )
    private List<Appointment> appointments = new ArrayList<>();

    public Calendar() {
    }

    public Calendar(List<Appointment> appointments) {
        this.appointments = appointments;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public List<Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<Appointment> appointments) {
        this.appointments = appointments;
    }


}
