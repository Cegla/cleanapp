package com.maciek.CleanApp.repositories;

import com.maciek.CleanApp.model.Apartment;
import com.maciek.CleanApp.model.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long> {


    List<Appointment> getAllByStartDateAfter(LocalDateTime startDate);

    @Query(value = "SELECT a FROM Appointment a WHERE a.startDate>?1")
    List<Appointment> getAllByDateGreaterThan(LocalDateTime startDate);




}
