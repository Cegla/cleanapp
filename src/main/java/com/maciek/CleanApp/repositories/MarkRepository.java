package com.maciek.CleanApp.repositories;

import com.maciek.CleanApp.model.Mark;
import com.maciek.CleanApp.model.MarkType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MarkRepository extends JpaRepository<Mark,Long> {



    List<Mark> getByScoreGreaterThanEqual(int score);





}
