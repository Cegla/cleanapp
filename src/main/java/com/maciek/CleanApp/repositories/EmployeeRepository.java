package com.maciek.CleanApp.repositories;

import com.maciek.CleanApp.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Long> {


    List<Employee> getAllBySurnameAndName(String surname, String name);

    List<Employee> getAllByMarks(Mark mark);


    @Query("select employee from Employee employee where employee.name = ?1")
    List<Employee> findByNameCustomQuery(String name);

    @Query("select e from Employee e join e.marks m where m.score >=?1")
    List<Employee> findByScoreHigherThanCustomQuery(Integer score);

    Employee getByLogin(String login);

    @Query("select e.calendar from Employee e where e.id=?1")
    Calendar findCalenderByEmployeeIdCustom(long id);


}
