package com.maciek.CleanApp.repositories;


import com.maciek.CleanApp.model.Employer;
import com.maciek.CleanApp.model.MarkType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface EmployerRepository extends JpaRepository<Employer,Long> {


    Employer getByLogin(String login);


    Employer getByLoginOrPassword(String login, String password);


    @Query("select employer from Employer employer join employer.marks mark where mark.score =?1")
    List<Employer> getAllByMarkType(int score);

    List<Employer> getAllBySurname(String surname);






}
