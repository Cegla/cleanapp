package com.maciek.CleanApp.repositories;


import com.maciek.CleanApp.model.Challenge;
import com.maciek.CleanApp.model.Employee;
import com.maciek.CleanApp.model.Employer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Repository
public interface ChallengeRepository extends JpaRepository<Challenge,Long> {

    List<Challenge> getByStatus(boolean status);

    List<Challenge> getByDateBetween(LocalDateTime startDate, LocalDateTime endDate);



    @Query("select c from Challenge c join c.apartment a  join a.employer e where e.id=?1")
    Challenge findChallengeByEmployerIdCustom(Long employerId);

    @Query("select  c from Challenge  c  join  c.apartment a where a.street =?1 and a.city=?2")
    List<Challenge> findChallengeByStreetAndCity(String street, String city);

    @Query("select  c from Challenge  c join c.apartment a where a.city=?1")
    List<Challenge> findChallengeByCity(String city);


    @Query("select  c from  Challenge c join c.apartment a join a.employer e where  e.name LIKE :name")
    List<Challenge> findChallengeByEmployerNameCustom(@Param("name") String name);

    @Query("select  c from  Challenge c join c.apartment a join a.employer e where  e.name=?1")
    List<Challenge> findChallengeByEmployerNameCustom2(String name);

    @Query("select  c from  Challenge c join c.apartment a join a.employer e  where  e.login=?1")
    List<Challenge> findChallengeByAccountLoginCustom(String login);

}
