package com.maciek.CleanApp.repositories;


import com.maciek.CleanApp.model.Offer;
import io.swagger.models.auth.In;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OfferRepository extends JpaRepository<Offer,Long> {

    List<Offer> getByChallenge_Id(Long id);

    List<Offer> getByRateIsGreaterThanEqual(Integer rate);

    List<Offer> getByEmployee_Surname(String surname);

    List<Offer> getByRateLessThan(Integer rate);



}
