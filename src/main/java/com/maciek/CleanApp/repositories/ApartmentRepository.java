package com.maciek.CleanApp.repositories;

import com.maciek.CleanApp.model.Apartment;
import com.maciek.CleanApp.model.Calendar;
import com.maciek.CleanApp.model.Challenge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;


@Repository
public interface ApartmentRepository extends JpaRepository<Apartment, Long> {


    @Query("select apartments from Apartment apartments where apartments.city =?1 ")
    List<Apartment> getApartmentsByCityCustomQuery(String city);

    Apartment getByChallenges(Challenge challenge);

    @Query("select  a from Apartment  a join a.challenges c where c.date >=?1")
    List<Apartment> getApartmentsWithChallengeDateGreaterThan(LocalDateTime date);

    @Query("select  a.calendar from Apartment a where a.id =?1")
    Calendar findCalendarByApartmentIdCustom(long id);



}
