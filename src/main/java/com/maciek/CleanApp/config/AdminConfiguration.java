package com.maciek.CleanApp.config;


import com.maciek.CleanApp.model.Employee;
import com.maciek.CleanApp.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class AdminConfiguration {


    private final EmployeeService employeeService;

    @Autowired
    public AdminConfiguration(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }


//    @PostConstruct
//    private void registerAdmin(){
//
//        Employee  admin = new Employee("admin", "superAdmin",
//                "Heniek", "Zwiewka","123997973");
//
//        employeeService.createEmployee(admin);
//
//    }



}
