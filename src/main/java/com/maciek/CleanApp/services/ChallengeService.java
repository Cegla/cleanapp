package com.maciek.CleanApp.services;


import com.maciek.CleanApp.model.Apartment;
import com.maciek.CleanApp.model.Challenge;
import com.maciek.CleanApp.repositories.ChallengeRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Service
public class ChallengeService {

    private final ChallengeRepository challengeRepository;
    private final ApartmentService apartmentService;

    public ChallengeService(ChallengeRepository challengeRepository, ApartmentService apartmentService) {
        this.challengeRepository = challengeRepository;
        this.apartmentService = apartmentService;
    }

    public Challenge createChallenge(Challenge challenge) {
        return challengeRepository.save(challenge);
    }

    public Challenge getById(Long id) {
        return challengeRepository.findOne(id);
    }

    public List<Challenge> getAll() {

        return challengeRepository.findAll();
    }

    public List<Challenge> getChallengesBetweenDates(LocalDateTime startDate, LocalDateTime endDate) {

        return challengeRepository.getByDateBetween(startDate, endDate);
    }

    public List<Challenge> getActiveChallenges() {
        return challengeRepository.getByStatus(true);
    }

    public List<Challenge> getNonActiveChallenges() {

        return challengeRepository.getByStatus(false);
    }

    public Challenge createChallengeForApartment(Challenge challenge, Long id) {

        Apartment apartmentById = apartmentService.getApartmentById(id);
        challenge.setApartment(apartmentById);

        return challengeRepository.save(challenge);


    }




}
