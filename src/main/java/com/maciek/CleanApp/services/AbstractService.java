package com.maciek.CleanApp.services;

import com.maciek.CleanApp.model.Mark;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class AbstractService {

    public Double generateAverageScore(List<Mark> marks){

        return marks.stream()
                .collect(Collectors.averagingInt(m -> m.getScore()));
    }



}
