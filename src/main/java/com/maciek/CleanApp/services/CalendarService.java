package com.maciek.CleanApp.services;


import com.maciek.CleanApp.model.Calendar;
import com.maciek.CleanApp.repositories.CalendarRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class CalendarService {

    private final CalendarRepository calendarRepository;
    private final ApartmentService apartmentService;
    private final EmployeeService employeeService;

    public CalendarService(CalendarRepository calendarRepository, ApartmentService apartmentService, EmployeeService employeeService) {
        this.calendarRepository = calendarRepository;
        this.apartmentService = apartmentService;
        this.employeeService = employeeService;
    }

    public Calendar createCalendar(Calendar calendar){
        return calendarRepository.save(calendar);
    }

    public Calendar getById(Long id){
        return calendarRepository.findOne(id);
    }


    public Calendar getByApartmentId(long id){
        return apartmentService.getCalendarByApartmentId(id);
    }

    public Calendar getByEmployeeId(long id){
        return employeeService.findCalendarByEmployeeId(id);
    }
}
