package com.maciek.CleanApp.services;


import com.maciek.CleanApp.model.Mark;
import com.maciek.CleanApp.repositories.MarkRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Transactional
@Service
public class MarkService {

    private final MarkRepository markRepository;




    public MarkService(MarkRepository markRepository) {
        this.markRepository = markRepository;

    }

    public Mark createMark(Mark mark){

        return markRepository.save(mark);

    }

    public List<Mark> getAll() {
        return markRepository.findAll();
    }




}
