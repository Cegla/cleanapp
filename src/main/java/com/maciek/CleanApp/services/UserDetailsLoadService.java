package com.maciek.CleanApp.services;


import com.maciek.CleanApp.model.Employee;
import com.maciek.CleanApp.model.Employer;
import com.maciek.CleanApp.repositories.EmployeeRepository;
import com.maciek.CleanApp.repositories.EmployerRepository;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service("userDetailsService")
@Transactional
public class UserDetailsLoadService implements UserDetailsService {


    private final EmployeeRepository employeeRepository;
    private final EmployerRepository employerRepository;

    public UserDetailsLoadService(EmployeeRepository employeeRepository, EmployerRepository employerRepository) {
        this.employeeRepository = employeeRepository;
        this.employerRepository = employerRepository;
    }


    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {


        Employee employee = employeeRepository.getByLogin(login);
        Employer employer = employerRepository.getByLogin(login);

        if (employee == null && employer == null) {
            throw new UsernameNotFoundException(String.format("User %s doesn't exists" + login));
        }

        if (employee != null) {

            return new User(employee.getLogin(), employee.getPassword(),
                    true, true, true, true, AuthorityUtils.createAuthorityList("USER"));
        }

        return new User(employer.getLogin(), employer.getPassword(),
                true, true, true, true, AuthorityUtils.createAuthorityList("USER"));
    }
}
