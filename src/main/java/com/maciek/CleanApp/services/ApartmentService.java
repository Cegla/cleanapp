package com.maciek.CleanApp.services;


import com.maciek.CleanApp.model.Apartment;
import com.maciek.CleanApp.model.Calendar;
import com.maciek.CleanApp.model.Challenge;
import com.maciek.CleanApp.model.Employer;
import com.maciek.CleanApp.repositories.ApartmentRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ApartmentService {

    private final ApartmentRepository apartmentRepository;
    private final EmployerService employerService;


    public ApartmentService(ApartmentRepository apartmentRepository, EmployerService employerService) {
        this.apartmentRepository = apartmentRepository;
        this.employerService = employerService;
    }


    public Apartment getApartmentById(Long id) {
        return apartmentRepository.findOne(id);
    }

    public Apartment createApartmentForEmployer(Long employerId, Apartment apartment) {
        Employer employer = employerService.getEmployerById(employerId);
        apartment.setEmployer(employer);

        return apartmentRepository.save(apartment);
    }

    public Calendar getCalendarByApartmentId(long id){
        return apartmentRepository.findCalendarByApartmentIdCustom(id);
    }

    public Apartment creteApartment(Apartment apartment){
        return apartmentRepository.save(apartment);
    }




    public List<Apartment> getAll() {

        return apartmentRepository.findAll();
    }

    public void destroy() {
        apartmentRepository.flush();
    }

}
