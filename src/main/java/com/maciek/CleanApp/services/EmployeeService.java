package com.maciek.CleanApp.services;

import com.maciek.CleanApp.exceptions.LoginExistsException;
import com.maciek.CleanApp.model.Calendar;
import com.maciek.CleanApp.model.Employee;
import com.maciek.CleanApp.model.Mark;
import com.maciek.CleanApp.repositories.EmployeeRepository;
import com.maciek.CleanApp.repositories.MarkRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final MarkRepository markRepository;
    private final PasswordEncoder encoder;

    public EmployeeService(EmployeeRepository employeeRepository, MarkRepository markRepository, PasswordEncoder encoder) {
        this.employeeRepository = employeeRepository;
        this.markRepository = markRepository;
        this.encoder = encoder;
    }

    public Employee createMarkForEmployee(Long employeeId, Mark mark) {
        Employee employee = employeeRepository.findOne(employeeId);
        List<Mark> marks = employee.getMarks();
        marks.add(mark);
        employee.setMarks(marks);

        return employeeRepository.save(employee);
    }



    public void flush(){
        employeeRepository.flush();
    }
    public void destroy(){
        employeeRepository.deleteAll();
    }



    public Employee createEmployee(Employee employee) {

        if(LoginOrPasswordExists(employee.getLogin())) {

            throw  new LoginExistsException(employee.getLogin());
        }


        employee.setPassword(encoder.encode(employee.getPassword()));


        return employeeRepository.save(employee);
    }

    public Employee getEmployeeById(Long id) {

        return employeeRepository.findOne(id);
    }

    public Calendar findCalendarByEmployeeId(long id){
        return employeeRepository.findCalenderByEmployeeIdCustom(id);
    }



    public List<Employee> getAll() {
        return employeeRepository.findAll();
    }

        private boolean LoginOrPasswordExists(String login) {

        boolean exists = (employeeRepository.getByLogin(login) != null);

        return exists;
    }


}


