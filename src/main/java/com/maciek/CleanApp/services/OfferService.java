package com.maciek.CleanApp.services;


import com.maciek.CleanApp.model.Challenge;
import com.maciek.CleanApp.model.Offer;
import com.maciek.CleanApp.repositories.ChallengeRepository;
import com.maciek.CleanApp.repositories.OfferRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class OfferService {


    private final OfferRepository offerRepository;
    private final ChallengeRepository challengeRepository;

    public OfferService(OfferRepository offerRepository, ChallengeRepository challengeRepository) {
        this.offerRepository = offerRepository;
        this.challengeRepository = challengeRepository;
    }

    public Offer createOffer(Offer offer) {
        return offerRepository.save(offer);
    }

    public List<Offer> getAll() {
        return offerRepository.findAll();
    }

    public List<Offer> getByChallengeId(Long challengeId) {
        return offerRepository.getByChallenge_Id(challengeId);
    }

    public List<Offer> getByRateGreaterEqualThan(Integer rate) {
        return offerRepository.getByRateIsGreaterThanEqual(rate);
    }

    public List<Offer> getByRateLessThan(Integer rate) {
        return offerRepository.getByRateLessThan(rate);
    }

    public List<Offer> getByEmployeeSurname(String surname) {

        return offerRepository.getByEmployee_Surname(surname);
    }

    public Offer getById(long id){
        return offerRepository.findOne(id);
    }


    public Offer createOfferForChallenge(Offer offer, Long challengeId){


        Challenge challenge = challengeRepository.findOne(challengeId);
        offer.setChallenge(challenge);

        return offerRepository.save(offer);

    }


}
