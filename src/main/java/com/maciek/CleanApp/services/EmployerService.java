package com.maciek.CleanApp.services;

import com.maciek.CleanApp.exceptions.LoginExistsException;
import com.maciek.CleanApp.model.Employer;
import com.maciek.CleanApp.model.Mark;
import com.maciek.CleanApp.repositories.EmployerRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class EmployerService {

    private final EmployerRepository employerRepository;
    private final MarkService markService;
    private final PasswordEncoder encoder;


    public EmployerService(EmployerRepository employerRepository, MarkService markService, PasswordEncoder encoder) {
        this.employerRepository = employerRepository;
        this.markService = markService;
        this.encoder = encoder;
    }

    public Employer createEmployer(Employer employer) {


        if(LoginOrPasswordExists(employer.getLogin())){
            throw  new LoginExistsException(employer.getLogin());
        }

        employer.setPassword(encoder.encode(employer.getPassword()));

        return employerRepository.save(employer);
    }



    public List<Employer> getAllBySurname(String surname) {
        return employerRepository.getAllBySurname(surname);
    }


    public Employer getEmployerById(Long id) {
        return employerRepository.findOne(id);
    }



    public Employer createMarkForEmployer(Mark mark, Long id) {

        Employer employer = employerRepository.findOne(id);


        List<Mark> marks = employer.getMarks();
        marks.add(mark);
        employer.setMarks(marks);

        return employerRepository.save(employer);


    }


    public List<Employer> createBatchOfEmployers(List<Employer> employers) {
        List<Employer> save = employerRepository.save(employers);

        return save;
    }

    public List<Employer> getAll() {
        return employerRepository.findAll();

    }

    public void destroy() {
        employerRepository.flush();
    }

    private boolean LoginOrPasswordExists(String login) {

        boolean exists = (employerRepository.getByLogin(login) != null);

        return exists;
    }

}
