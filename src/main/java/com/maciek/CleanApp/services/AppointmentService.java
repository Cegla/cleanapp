package com.maciek.CleanApp.services;

import com.maciek.CleanApp.model.Appointment;
import com.maciek.CleanApp.repositories.AppointmentRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
public class AppointmentService {

    private final AppointmentRepository appointmentRepository;


    public AppointmentService(AppointmentRepository appointmentRepository) {
        this.appointmentRepository = appointmentRepository;
    }

    public Appointment getAppointmentById(long id) {
        return appointmentRepository.findOne(id);
    }

    public Appointment createAppointment(Appointment appointment) {
        return appointmentRepository.save(appointment);
    }

    public List<Appointment> getAll() {

        return appointmentRepository.findAll();
    }


    public LocalDateTime generateEndDate(LocalDateTime date, Integer duration) {

        return date.plusHours(duration.longValue());
    }
}
