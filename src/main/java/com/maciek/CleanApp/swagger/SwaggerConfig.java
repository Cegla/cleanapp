package com.maciek.CleanApp.swagger;


import jdk.nashorn.internal.runtime.regexp.joni.Regex;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket customImplementation(){

        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.maciek.CleanApp"))
                .paths(PathSelectors.regex("/.*"))
                .build()
                .apiInfo(infoAboutAPI());

    }

    private ApiInfo infoAboutAPI() {

    return new ApiInfoBuilder()
            .title("Clean App")
            .description("Controllers")
            .version("0.0.1")
            .contact(new Contact("Maciek" ,"Clean.app.com", "maciek@gmail.com"))
            .build();
    }
}
