package com.maciek.CleanApp.controllersAdvice;


import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestControllerAdvice
public class CreatingClientAdvise extends Exception {

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public List<Object> clientValidationHandler(MethodArgumentNotValidException e){


        List<Object> errorResponse = new ArrayList<>();
        List<ObjectError> allErrors = e.getBindingResult().getAllErrors();

        for (ObjectError error: allErrors)
              {
                  Map<String,String> errorItem = new HashMap<>();
                  errorItem.put("message",error.getDefaultMessage());
                  errorResponse.add(errorItem);

        }

        return errorResponse;

    }
}
