package com.maciek.CleanApp.controllers;

import com.maciek.CleanApp.dto.EmployeeDto;
import com.maciek.CleanApp.services.EmployeeService;
import com.maciek.CleanApp.model.Employee;
import com.maciek.CleanApp.services.MarkService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;

import static com.maciek.CleanApp.mappers.EmployeeToEmployeeDtoMapper.mapEmployeesToEmployeeDtos;
import static com.maciek.CleanApp.mappers.EmployeeToEmployeeDtoMapper.mapperEmployeeToEmployeeDto;
import static com.maciek.CleanApp.utils.UriGeneratorUtil.generatePathForId;

@RestController
@RequestMapping("employee")
public class EmployeeController {


    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }


    @PostMapping()
    public ResponseEntity<Void> createEmployee(@Valid @RequestBody Employee employee) {

        Employee serviceEmployee = employeeService.createEmployee(employee);

        return ResponseEntity.created(generatePathForId(serviceEmployee.getId())).build();

    }

    @GetMapping("{id}")
    public ResponseEntity<EmployeeDto> getEmployeeWithAverage(@PathVariable String id) {
        Long idLong = Long.parseLong(id);

        Employee employeeById = employeeService.getEmployeeById(idLong);

        if (employeeById == null){
            return ResponseEntity.noContent().build();
        }

        EmployeeDto employeeDto = mapperEmployeeToEmployeeDto(employeeById);

        return ResponseEntity.ok(employeeDto);
    }


    @GetMapping("/all")
    public ResponseEntity<List<EmployeeDto>> getAllEmployee(){

        List<Employee> employees = employeeService.getAll();

        if(employees.isEmpty()){
            return ResponseEntity.noContent().build();
        }

        List<EmployeeDto> employeeDtoList = mapEmployeesToEmployeeDtos(employees);

        return ResponseEntity.ok(employeeDtoList);
    }


}
