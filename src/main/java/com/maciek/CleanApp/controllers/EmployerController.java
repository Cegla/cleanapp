package com.maciek.CleanApp.controllers;


import com.maciek.CleanApp.dto.EmployerDto;
import com.maciek.CleanApp.services.AbstractService;
import com.maciek.CleanApp.services.ApartmentService;
import com.maciek.CleanApp.services.EmployerService;
import com.maciek.CleanApp.model.Apartment;
import com.maciek.CleanApp.model.Employer;
import com.maciek.CleanApp.services.MarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static com.maciek.CleanApp.mappers.EmployerToEmployerDtoMapper.mapEmployersToEmployersDtos;
import static com.maciek.CleanApp.mappers.EmployerToEmployerDtoMapper.mapperEmployerToEmployerDto;
import static com.maciek.CleanApp.utils.UriGeneratorUtil.generatePathForId;

@RestController
@RequestMapping(value="employer")
public class EmployerController {

    private final EmployerService employerService;
    private final ApartmentService apartmentService;


    @Autowired
    public EmployerController(EmployerService employerService, ApartmentService apartmentService) {
        this.employerService = employerService;
        this.apartmentService = apartmentService;
    }


    @PostMapping
    public ResponseEntity<Void> createEmployer(@Valid @RequestBody Employer employer){

        Employer createdEmployer = employerService.createEmployer(employer);

        return ResponseEntity.created((generatePathForId(createdEmployer.getId()))).build();
    }



    @PostMapping("{id}/apartment")
    
    public ResponseEntity<Void> createApartmentForEmployer(@Valid @RequestBody Apartment apartment, @PathVariable Long id){

        Apartment saved = apartmentService.createApartmentForEmployer(id, apartment);

        return ResponseEntity.created(generatePathForId(saved.getId())).build();
    }

    @GetMapping("{id}")
    public ResponseEntity<EmployerDto> getEmployerWithAverage(@PathVariable Long id){

        Employer employerById = employerService.getEmployerById(id);

        if(employerById == null){
            return ResponseEntity.noContent().build();
        }
        EmployerDto employerDto = mapperEmployerToEmployerDto(employerById);

        return ResponseEntity.ok(employerDto);

    }

    @PostMapping("/all")
    public ResponseEntity<Void> createBatch(@RequestBody List<Employer> employerList)
    {
        List<Employer> batchOfEmployers = employerService.createBatchOfEmployers(employerList);
        return ResponseEntity.created(URI.create("/localhost:8080/employer/all")).build();
    }

    @GetMapping("/all")
    public ResponseEntity<List<EmployerDto>> getAllEmployers(){
        List<Employer> employers = employerService.getAll();

        if (employers.isEmpty()){
            return ResponseEntity.noContent().build();
        }

        List<EmployerDto> employerDtoList = mapEmployersToEmployersDtos(employers);

        return ResponseEntity.ok(employerDtoList);

    }




}
