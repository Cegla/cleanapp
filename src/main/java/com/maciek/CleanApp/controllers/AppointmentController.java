package com.maciek.CleanApp.controllers;

import com.maciek.CleanApp.exceptions.NoSuchEntityException;
import com.maciek.CleanApp.model.*;
import com.maciek.CleanApp.repositories.CalendarRepository;
import com.maciek.CleanApp.services.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static com.maciek.CleanApp.utils.UriGeneratorUtil.generatePathForId;

@RestController
@RequestMapping("appointments")
public class AppointmentController {

    private final AppointmentService appointmentService;
    private final ApartmentService apartmentService;
    private final ChallengeService challengeService;
    private final OfferService offerService;
    private final CalendarService calendarService;
    private final EmployeeService employeeService;

    public AppointmentController(AppointmentService appointmentService, ApartmentService apartmentService, ChallengeService challengeService, OfferService offerService, CalendarService calendarService, EmployeeService employeeService) {
        this.appointmentService = appointmentService;
        this.apartmentService = apartmentService;
        this.challengeService = challengeService;
        this.offerService = offerService;
        this.calendarService = calendarService;
        this.employeeService = employeeService;
    }


    @GetMapping("{id}")
    public ResponseEntity<Appointment> getAppointment(@PathVariable long id) {

        Appointment appointment = appointmentService.getAppointmentById(id);

        if (appointment == null) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(appointment);
    }


    @PostMapping()
    public ResponseEntity<Void> createAppointment(@RequestParam long challengeId, @RequestParam long offerId) {


        Challenge challenge = challengeService.getById(challengeId);
        if (challenge == null) {

            throw new NoSuchEntityException(String.format("There is no entity %s with id %s in the database", Challenge.class.getName().substring(26), challengeId));

        }

        Offer offer = offerService.getById(offerId);
        if (offer == null) {
            throw new NoSuchEntityException(String.format("There is no entity %s with id %s in the database", Offer.class.getName().substring(26), offerId));

        }

        LocalDateTime endDate = appointmentService.generateEndDate(challenge.getDate(), offer.getDuration());
        Appointment appointment = appointmentService.createAppointment(new Appointment(challenge.getDate(), endDate, offer.getRate(), AppointmentStatus.SIGNED));



        Apartment apartment = challenge.getApartment();
        Employee employee = offer.getEmployee();



        if (apartment == null) {

            throw new NoSuchEntityException(String.format("There is no entity %s with id %s in the database", Apartment.class.getName().substring(26), apartment.getId()));
        }

        if (employee == null) {

            throw new NoSuchEntityException(String.format("There is no entity %s with id %s in the database", Employee.class.getName().substring(26), employee.getId()));

        }


        addAppointmentToEmployeeCalendar(appointment, employee);
        addAppointmentToApartmentCalendar(appointment, apartment);


        return ResponseEntity.created(generatePathForId(appointment.getId())).build();


    }



    private void addAppointmentToApartmentCalendar(Appointment appointment, Apartment apartment) {
        Calendar calendar = apartment.getCalendar();
        if (calendar != null) {
            addNewAppointment(appointment, calendar);
        } else {
            apartment.setCalendar(calendar);
            apartmentService.creteApartment(apartment);
        }
    }


    private void addAppointmentToEmployeeCalendar(Appointment appointment, Employee employee) {
        Calendar calendar = employee.getCalendar();
        if (calendar != null) {
            addNewAppointment(appointment, calendar);
        } else {

            calendar =  new Calendar();
            calendar.setAppointments(Arrays.asList(appointment));
            calendarService.createCalendar(calendar);
            employee.setCalendar(calendar);

        }

    }


    private Calendar addNewAppointment(Appointment appointment, Calendar calendar) {
        List<Appointment> appointments = calendar.getAppointments();
        appointments.add(appointment);
        calendar.setAppointments(appointments);

        return calendarService.createCalendar(calendar);
    }

}
