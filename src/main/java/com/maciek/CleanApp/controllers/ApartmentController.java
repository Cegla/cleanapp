package com.maciek.CleanApp.controllers;


import com.maciek.CleanApp.model.Challenge;
import com.maciek.CleanApp.services.ApartmentService;
import com.maciek.CleanApp.model.Apartment;
import com.maciek.CleanApp.services.ChallengeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

import static com.maciek.CleanApp.utils.UriGeneratorUtil.generatePathForId;

@RestController
@RequestMapping("apartments")
public class ApartmentController {

    private final ApartmentService apartmentService;

    private final ChallengeService challengeService;


    @Autowired
    public ApartmentController(ApartmentService apartmentService, ChallengeService challengeService) {
        this.apartmentService = apartmentService;
        this.challengeService = challengeService;
    }

    @GetMapping("{id}")
    public ResponseEntity<Apartment> getApartmentById(@PathVariable Long id) {
        Apartment Apartment = apartmentService.getApartmentById(id);

        if (Apartment == null) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok(Apartment);
        }
    }


    @GetMapping("/all")
    public ResponseEntity<List<Apartment>> getAllApartments() {
        List<Apartment> apartments = apartmentService.getAll();

        if (apartments.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(apartments);

    }


    @PostMapping("{id}/challenge")
    public ResponseEntity<Void> createChallengeForApartment(@PathVariable Long id,@Valid Challenge challenge ){

        Challenge challengeForApartment = challengeService.createChallengeForApartment(challenge, id);

        return ResponseEntity.created(generatePathForId(challengeForApartment.getId())).build();
    }





}
