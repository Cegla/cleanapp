package com.maciek.CleanApp.controllers;


import com.maciek.CleanApp.model.Challenge;
import com.maciek.CleanApp.model.Offer;
import com.maciek.CleanApp.services.ChallengeService;
import com.maciek.CleanApp.services.OfferService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.maciek.CleanApp.utils.UriGeneratorUtil.generatePathForId;

@RestController
@RequestMapping("offers")
public class OfferController {

    private final OfferService offerService;
    private final ChallengeService challengeService;

    public OfferController(OfferService offerService, ChallengeService challengeService) {
        this.offerService = offerService;
        this.challengeService = challengeService;
    }



}
