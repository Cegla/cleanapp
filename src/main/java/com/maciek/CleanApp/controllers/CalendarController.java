package com.maciek.CleanApp.controllers;


import com.maciek.CleanApp.model.Calendar;
import com.maciek.CleanApp.services.ApartmentService;
import com.maciek.CleanApp.services.CalendarService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/calendar")
public class CalendarController {

    private final CalendarService calendarService;
    private final ApartmentService apartmentService;

    public CalendarController(CalendarService calendarService, ApartmentService apartmentService) {
        this.calendarService = calendarService;
        this.apartmentService = apartmentService;
    }

    @GetMapping("/apartment/{id}")
    public ResponseEntity<Calendar> getCalendarByApartmentId(@PathParam("id") long id) {

        Calendar calendar = apartmentService.getCalendarByApartmentId(id);

        if (calendar == null)
        {
            return ResponseEntity.noContent().build();
        }


            return ResponseEntity.ok(calendar);
    }

    @GetMapping("/employee/{id}")
    public ResponseEntity<Calendar> getCalendarByEmployeeId(@PathParam("id") long id) {

        Calendar calendar = calendarService.getByEmployeeId(id);

        if (calendar == null)
        {
            return ResponseEntity.noContent().build();
        }


            return ResponseEntity.ok(calendar);
    }



}
