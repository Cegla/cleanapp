package com.maciek.CleanApp.controllers;

import com.maciek.CleanApp.model.Challenge;
import com.maciek.CleanApp.model.Offer;
import com.maciek.CleanApp.services.ChallengeService;
import com.maciek.CleanApp.services.OfferService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.maciek.CleanApp.utils.UriGeneratorUtil.generatePathForId;

@RestController
@RequestMapping("challenge")
public class ChallengeController {

    private final ChallengeService challengeService;
    private final OfferService offerService;


    public ChallengeController(ChallengeService challengeService, OfferService offerService) {
        this.challengeService = challengeService;
        this.offerService = offerService;
    }


    @GetMapping("{id}")
    public ResponseEntity<Challenge> getChallenge(@PathVariable Long id){

        Challenge challenge = challengeService.getById(id);

        if(challenge == null){
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(challenge);


    }


    @GetMapping("/all")
    public ResponseEntity<List<Challenge>> getOffersForChallenge(){

        List<Challenge> all = challengeService.getAll();

        if (all.isEmpty()){
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(all);
    }

    @GetMapping("{id}/offers")
    public ResponseEntity<List<Offer>> getOffersForChallenge(@PathVariable Long id){

        Challenge challenge = challengeService.getById(id);
        List<Offer> offers = offerService.getByChallengeId(challenge.getId());

        if (offers.isEmpty()){
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(offers);
    }

    @PostMapping("{id}/offers")
    public ResponseEntity<Void> createOfferForChallenge(@Valid @RequestBody Offer offer, @PathVariable Long id){

        Challenge byId = challengeService.getById(id);
        if (byId == null){
            return ResponseEntity.noContent().build();
        }

        Offer challenge = offerService.createOfferForChallenge(offer, id);

        return ResponseEntity.created(generatePathForId(challenge.getId())).build();

    }





}
