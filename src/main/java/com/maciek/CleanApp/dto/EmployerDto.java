package com.maciek.CleanApp.dto;

import com.maciek.CleanApp.model.Apartment;
import com.maciek.CleanApp.model.Challenge;
import com.maciek.CleanApp.model.Mark;

import java.util.ArrayList;
import java.util.List;

public class EmployerDto {

    protected Long id;

    private String login;


    private String password;

    protected String name;

    protected String surname;

    protected String phoneNo;

    private List<Apartment> apartments = new ArrayList<>();

    private List<Mark> marks = new ArrayList<>();


    private List<Challenge> challenges;

    private Double scoreAverage;


    public EmployerDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public List<Apartment> getApartments() {
        return apartments;
    }

    public void setApartments(List<Apartment> apartments) {
        this.apartments = apartments;
    }

    public List<Mark> getMarks() {
        return marks;
    }

    public void setMarks(List<Mark> marks) {
        this.marks = marks;
    }


    public Double getScoreAverage() {
        return scoreAverage;
    }

    public void setScoreAverage(Double scoreAverage) {
        this.scoreAverage = scoreAverage;
    }

    public List<Challenge> getChallenges() {
        return challenges;
    }

    public void setChallenges(List<Challenge> challenges) {
        this.challenges = challenges;
    }

    @Override
    public String toString() {
        return "EmployerDto{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", phoneNo='" + phoneNo + '\'' +
                ", apartments=" + apartments +
                ", marks=" + marks +
                ", challenges=" + challenges +
                ", scoreAverage=" + scoreAverage +
                '}';
    }

}
