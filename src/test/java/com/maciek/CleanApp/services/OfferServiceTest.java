package com.maciek.CleanApp.services;

import com.maciek.CleanApp.model.Apartment;
import com.maciek.CleanApp.model.Challenge;
import com.maciek.CleanApp.model.Employer;
import com.maciek.CleanApp.model.Offer;
import com.maciek.CleanApp.repositories.ApartmentRepository;
import com.maciek.CleanApp.repositories.ChallengeRepository;
import com.maciek.CleanApp.repositories.EmployerRepository;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OfferServiceTest {

    @Autowired
    private OfferService offerService;



    @Autowired
    private ChallengeRepository challengeRepository;



    @Autowired
    private ApartmentRepository apartmentRepository;


    private Offer offer;
    private Challenge stillActive;
    private Challenge notActive;
    private Boolean expired;
    private Boolean active;


    private Apartment stajnia;



    @Before
    public void SetUp(){


        expired = false;
        active = true;


        offer = new Offer(120,null,null);
        stajnia = apartmentRepository.save(apartmentRepository.save(new Apartment("Stajnia", 4, "Gnojna", "Athens",null )));

        stillActive = challengeRepository.save(new Challenge(null, expired,stajnia, null));
        notActive = challengeRepository.save(new Challenge(null, active, null, null));

    }


    @Test
    public void createOfferForChallenge() throws Exception {

        //given setUp

        //when
        Offer offer = offerService.createOfferForChallenge(this.offer, stajnia.getId());

        //then
        Assertions.assertThat(offer).isNotNull();
        Assertions.assertThat(offer.getRate()).isEqualTo(120);
    }

}