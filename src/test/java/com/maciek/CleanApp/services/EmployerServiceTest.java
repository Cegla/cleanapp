package com.maciek.CleanApp.services;

import com.maciek.CleanApp.CleanAppApplication;
import com.maciek.CleanApp.model.Employer;
import com.maciek.CleanApp.model.Mark;
import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = CleanAppApplication.class)
public class EmployerServiceTest {


    @Autowired
    private EmployerService employerService;



    private Employer misza;

    private Mark slabo;



    @Before
    public void setup(){

        misza = employerService.createEmployer(new Employer("Misza", "Raskolnikov", "Grisza", "Raskolnikov","123456789",null,null));
        slabo = new Mark("Kiepawo", 1);

    }

    @After
    public void tearDown(){
        employerService.destroy();
    }



    @Test
    public void shouldCreateMarkForEmployer() throws Exception {

        //given setup


        //when
        Employer employer = employerService.createMarkForEmployer(slabo, misza.getId());

        //then
        Assertions.assertThat(employer).isNotNull();
        Assertions.assertThat(employer.getMarks().get(0).getDescription()).isEqualTo(slabo.getDescription());
    }

    @Test
    public void shouldCreateSecondMarkForEmployer() throws Exception {

        //given setup
        Mark superJee = new Mark("Mega", 5);
        Mark superHurra = new Mark("MegaMega", 5);
        Employer wania = employerService.createEmployer(new Employer("Wania", "Raskolnikov", "Grisza", "Raskolnikov","123456789",null,null));


        //when
        employerService.createMarkForEmployer(superHurra, wania.getId());
        Employer employer1 = employerService.createMarkForEmployer(superJee, wania.getId());

        //then

        Assertions.assertThat(employer1.getMarks()).isNotEmpty();
        Assertions.assertThat(employer1.getMarks().get(0).getDescription()).isEqualTo(superHurra.getDescription());
        Assertions.assertThat(employer1.getMarks().get(1).getDescription()).isEqualTo(superJee.getDescription());
        Assertions.assertThat(employer1.getMarks().size()).isEqualTo(2);




    }

}