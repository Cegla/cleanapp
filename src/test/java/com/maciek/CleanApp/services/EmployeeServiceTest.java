package com.maciek.CleanApp.services;

import com.maciek.CleanApp.model.Employee;
import com.maciek.CleanApp.model.Mark;
import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;


@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class EmployeeServiceTest {


    @Autowired
    MarkService markService;

    @Autowired
    EmployeeService employeeService;

    Mark mark1;
    Mark mark2;
    Mark mark3;

    Employee heniek;



    @After
    public void TearDown(){
        employeeService.destroy();
    }


    @Test
    public void shouldCreateMarkForEmployee() throws Exception {

        //given

        Mark newMark = new Mark("Ok" ,3);

        //when


        Employee employee = employeeService.createMarkForEmployee(heniek.getId(), newMark);

        //then

        Assertions.assertThat(employee).isNotNull();
        Assertions.assertThat(employee.getMarks().get(3).getDescription()).isEqualTo(newMark.getDescription());

    }
        @Test
    public void shouldCreateOneEmployee() throws Exception {

        //given

         Employee Wini = new Employee("Winnie", "the pooh", "Winnie", "the pooh","123997973", null,  Arrays.asList(mark1, mark2, mark3));


        //when


        Employee employee = employeeService.createEmployee(Wini);

        //then

        Assertions.assertThat(employee).isNotNull();
//        Assertions.assertThat(employee.getMarks().get(3).getDescription()).isEqualTo(newMark.getDescription());

    }






}