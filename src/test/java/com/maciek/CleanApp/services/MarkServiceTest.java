package com.maciek.CleanApp.services;

import com.maciek.CleanApp.CleanAppApplication;
import com.maciek.CleanApp.model.Employee;
import com.maciek.CleanApp.model.Mark;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CleanAppApplication.class)
public class MarkServiceTest {

    @Autowired
    MarkService markService;

    @Autowired
    AbstractService abstractService;

    @Autowired
     EmployeeService employeeService;

    Mark mark1;
    Mark mark2;
    Mark mark3;

    Employee heniek;

    @Before
    public void setUp(){

        mark1 = new Mark("Super", 5);
        mark2 = new Mark("Raczej średnio", 4);
        mark3 = new Mark("Słabo ", 3);

        heniek = new Employee("Heniek", "Zwiewka","Heniek", "Zwiewka", "123997973", null, Arrays.asList(mark1, mark2, mark3));

        employeeService.createEmployee(heniek);

    }

    @Test
    public void getAverageScoreForEmployee() throws Exception {

        //given setUp



        //when
        Double averageScoreForEmployee = abstractService.generateAverageScore(heniek.getMarks());

        //then

        Assertions.assertThat(averageScoreForEmployee).isNotNaN();
        Assertions.assertThat(averageScoreForEmployee).isEqualTo(4.0);


    }



}