package com.maciek.CleanApp.mappers;

import com.maciek.CleanApp.dto.EmployeeDto;
import com.maciek.CleanApp.model.Employee;
import com.maciek.CleanApp.model.Mark;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.maciek.CleanApp.mappers.EmployeeToEmployeeDtoMapper.mapEmployeesToEmployeeDtos;

public class EmployeeToEmployeeDtoMapperTest {



    private Employee jurek;
    private Employee marek;
    private Mark mark1;
    private Mark mark2;
    private Mark mark3;



    @Before
    public void setup(){

        mark1 = new Mark("Super", 6);
        mark2 = new Mark("Slabo", 4);
        mark3 = new Mark("Zena", 2);


        jurek = new Employee("Jurek", "Ogorek", "Kielbasa", "i Sznurek", "123456789");
        jurek.setMarks(Arrays.asList(mark1,mark2,mark3));

        marek = new Employee("Marek", "Talarek", "Maro", "za Talaro", "123456789");
        marek.setMarks(Arrays.asList(mark1,mark3));


    }

    @Test
    public void shouldGetEmployeeDtos() throws Exception {


        //given setUp

        //when

        List<EmployeeDto> employeeDtoList = mapEmployeesToEmployeeDtos(Arrays.asList(jurek, marek));

        //then

        Assertions.assertThat(employeeDtoList).isNotEmpty();
        Assertions.assertThat(employeeDtoList.get(1).getScoreAverage()).isEqualTo(4);
        Assertions.assertThat(employeeDtoList.get(0).getScoreAverage()).isEqualTo(4);



    }

}