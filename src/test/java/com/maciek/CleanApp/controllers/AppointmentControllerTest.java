package com.maciek.CleanApp.controllers;

import com.maciek.CleanApp.CleanAppApplication;
import com.maciek.CleanApp.model.*;
import com.maciek.CleanApp.services.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.notNullValue;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = CleanAppApplication.class)
public class AppointmentControllerTest {


    @LocalServerPort
    private Integer port;

    @Autowired
    private EmployeeService employeeService;


    @Autowired
    private EmployerService employerService;

    @Autowired
    private ApartmentService apartmentService;

    @Autowired
    private OfferService offerService;


    @Autowired
    private ChallengeService challengeService;

    private Challenge challenge;


    private Offer offer;


    private Apartment kamienna;


    private Employer Zenek;

    private Employee jacek;

    LocalDateTime appointmentStartDate;
    LocalDateTime appointmentEndtDate;
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    @Before
    public void setup() {

        appointmentStartDate = LocalDateTime.parse("2017-10-10 09:00", formatter);

        jacek = employeeService.createEmployee(new Employee("Heniek", "Zwiewka", "Heniek", "Zwiewka","123997973"));

        Zenek = employerService.createEmployer(new Employer("Zenek", "Niezgoda", "Zenek", "Niezgoda", "123456789", null, null));

        kamienna = apartmentService.createApartmentForEmployer(Zenek.getId(), new Apartment("kamienna", 3, "Niepodleglosci", "Warsaw", null));

        challenge = challengeService.createChallengeForApartment(new Challenge(appointmentStartDate, true, null, null), kamienna.getId());

        offer = offerService.createOfferForChallenge(new Offer(120, 2,jacek, null), challenge.getId());



    }


    @Test
    public void shouldCreateAppointment() throws Exception {


        //@formatter:off
    given()
            .port(port)
            .authentication().preemptive().basic("admin", "superAdmin")
            .param("challengeId",challenge.getId())
            .param("offerId", offer.getId())
            .log().all()
    .when()
            .post("/appointments")
    .then()
            .log().all()
            .assertThat()
            .statusCode(201)
            .header("Location",notNullValue());
        //@formatter:on
    }





    @Test
    public void shouldGetOneAppointment() throws Exception {

        //given
        long id = 1;

        //@formatter:off

        given()
                .port(port)
                .authentication().preemptive().basic("admin", "superAdmin")
                .pathParam("id",id)
                .log().all()
        .when()
                .get("/appointments/{id}")
         .then()
                .assertThat()
                .log().all()
                .statusCode(200);

                //@formatter:on

    }


}