package com.maciek.CleanApp.controllers;

import com.maciek.CleanApp.CleanAppApplication;
import com.maciek.CleanApp.model.Apartment;
import com.maciek.CleanApp.model.Employer;
import com.maciek.CleanApp.services.EmployeeService;
import com.maciek.CleanApp.services.EmployerService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.is;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = CleanAppApplication.class)
public class EmployerControllerTest {


    @Autowired
    EmployerService employerService;

    Employer zenek;
    Apartment apartamentZenka;


    @LocalServerPort
    Integer port;

    @Before
    public void setUp() {

        apartamentZenka = new Apartment("Zenkow Jama", 1, null, null, null);
        zenek = new Employer("Zenek", "Niezgoda","Zenek", "Niezgoda", "123456789", null, null);
        employerService.createEmployer(zenek);


    }

    @Test
    public void shouldCreateApartmentForEmployer() throws Exception {

        //@formatter:off
        given()
                .port(port)
                .auth().preemptive().basic("admin","superAdmin")
                .log().all()
                .body(apartamentZenka)
                .header(CONTENT_TYPE,APPLICATION_JSON_VALUE)
        .when()
                .post("employer/"+zenek.getId()+"/apartment")
        .then()
               .log().all()
               .assertThat()
               .statusCode(201)
                .header("Location", notNullValue());

        //@formatter:on




    }

    @Test
    public void shouldCreateEmployer() throws Exception {

        Employer employer = new Employer("Zenek", "Niezgoda", "Zenek", "Niezgoda","123456789",null,null);


        //@formatter:off
        given()
                .port(port)
                .auth().preemptive().basic("admin","superAdmin")
                .log().all()
                .body(employer)
                .header(CONTENT_TYPE,APPLICATION_JSON_VALUE)

        .when()
                .post("/employer")

         .then()
                .log().all()

                .assertThat()
                .statusCode(201)
                .header("Location", notNullValue());
        //@formatter:on


    }


}