package com.maciek.CleanApp.controllers;

import com.maciek.CleanApp.CleanAppApplication;
import com.maciek.CleanApp.model.Apartment;
import com.maciek.CleanApp.model.Challenge;
import com.maciek.CleanApp.model.Employer;
import com.maciek.CleanApp.services.ApartmentService;
import com.maciek.CleanApp.services.EmployerService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = CleanAppApplication.class)
public class ApartmentControllerTest {


    @LocalServerPort
    private Integer port;

    private Employer misio;
    private Apartment kamienna;
    private Challenge challenge;
    private LocalDateTime newDate;
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    @Autowired
    private ApartmentService apartmentService;

    @Autowired
    private EmployerService employerService;

    @Before
    public void setUp() throws Exception {



        newDate = LocalDateTime.parse("2017-10-09 09:00",formatter);

        challenge = new Challenge(newDate,true,null,null);
        misio = employerService.createEmployer(new Employer(
                "Misiu","Zdzisiu123","Zdzislaw", "Zwiewka", "123456789", null, null));
        kamienna = apartmentService.createApartmentForEmployer(misio.getId(), new Apartment("kamienna", 3, "Niepodleglosci", "Warsaw", null));
    }

    @After
    public void tearDown(){
        employerService.destroy();
        apartmentService.destroy();
    }

    @Test
    public void shouldCreateChallengeForApartment() throws Exception {

          //@formatter:off
        given()
                .port(port)
                .auth().preemptive().basic("admin","superAdmin")
                .pathParam("id", kamienna.getId())
                .body(challenge)
                .header(CONTENT_TYPE,APPLICATION_JSON_VALUE)
                .log().all()
         .when()
                .post("apartments/{id}/challenge")
         .then()
                .log().all()
                .assertThat()
                .statusCode(201)
                .header("Location",notNullValue());

            //@formatter:on

    }

    @Test
    public void ShouldGetApartmentsById() throws Exception {


        //@formatter:off
        given()
                .port(port)
                .auth().preemptive().basic("admin","superAdmin")
                .pathParam("id", misio.getId())
                .log().all()
         .when()
                .get("apartments/{id}")
         .then()
                .log().all()
                .assertThat()
                        .statusCode(200)
                        .body("name", is("kamienna"))
                        .body("roomNumbers", is(3));

//@formatter:on

    }

    @Test
    public void ShouldThrowMessageViaClientValidationHandler1() {

        //given
        Employer henio = new Employer("Henio", "Enriques", "Henio", "Enriques","12", null, null);

        //@formatter:off
    given()
            .port(port)
            .auth().preemptive().basic("Misiu","Zdzisiu123")
            .log().all()
            .body(henio)
            .header(CONTENT_TYPE,APPLICATION_JSON_VALUE)
     .when()
            .post("employer")

      .then()
            .log().all()
            .assertThat()
                    .statusCode(400)
                    .body("message", is(Arrays.asList("Phone number can not have less than nine digits")));



    //@formatter:on
    }
    @Test
    public void ShouldThrowMessageViaClientValidationHandler2() {

        //given
        Employer henio = new Employer("H", "Enriques","Henio", "Enriques", "12", null, null);
        List<String> errorMessages = Arrays.asList("Phone number can not have less than nine digits","Name can not have less than two chars" );

        //@formatter:off
    given()
            .port(port)
            .auth().preemptive().basic("admin","superAdmin")
            .log().all()
            .body(henio)
            .header(CONTENT_TYPE,APPLICATION_JSON_VALUE)
     .when()
            .post("employer")

      .then()
            .log().all()
            .assertThat()
                    .statusCode(400)
                    .body("message", is(Arrays.asList("Phone number can not have less than nine digits")))
                    .body("message", is(Arrays.asList("Name can not have less than two chars")));



    //@formatter:on
    }





}