package com.maciek.CleanApp.controllers;

import com.maciek.CleanApp.CleanAppApplication;
import com.maciek.CleanApp.model.Challenge;
import com.maciek.CleanApp.model.Offer;
import com.maciek.CleanApp.repositories.ChallengeRepository;
import com.maciek.CleanApp.services.ChallengeService;
import com.maciek.CleanApp.services.OfferService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static com.google.common.net.HttpHeaders.CONTENT_TYPE;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.*;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = CleanAppApplication.class)
public class ChallengeControllerTest {


    @LocalServerPort
    Integer port;

    @Autowired
    private OfferService offerService;

    @Autowired
    private ChallengeService challengeService;
    private Offer offer;

    private Challenge oneChallenge;

    private LocalDateTime startDate;
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    @Before
    public void setup() {

       offer = new Offer(120,null,null);

        LocalDateTime startDate = LocalDateTime.parse("2017-10-05 09:00",formatter);


        oneChallenge = challengeService.createChallenge (new Challenge(startDate, true, null, null));


    }

    @Test
    public void getOffersForChallenge() throws Exception {
    }

    @Test
    public void shouldCreateOfferForChallenge() throws Exception {


        //@formatter:off
        given()
                .port(port)
                .auth().preemptive().basic("admin","superAdmin")
                .pathParam("id",oneChallenge.getId())
                .body(offer)
                .header(CONTENT_TYPE,APPLICATION_JSON_VALUE)

         .when()
                .post("/challenge/{id}/offers")
          .then()
                .log().all()
                .assertThat()
                .statusCode(201)
                .header("Location",notNullValue());
         //@formatter:on

    }

}