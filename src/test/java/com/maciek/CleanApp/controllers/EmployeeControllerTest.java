package com.maciek.CleanApp.controllers;

import com.maciek.CleanApp.CleanAppApplication;
import com.maciek.CleanApp.model.Employee;
import com.maciek.CleanApp.model.Mark;
import com.maciek.CleanApp.services.EmployeeService;
import com.maciek.CleanApp.services.MarkService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.when;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = CleanAppApplication.class)
public class EmployeeControllerTest {

    @LocalServerPort
    private Integer port;


    @Autowired
    EmployeeController employeeController;


    @Autowired
    MarkService markService;

    @Autowired
    EmployeeService employeeService;

    Mark mark1;
    Mark mark2;
    Mark mark3;

    Employee heniek;

    @Before
    public void setUp() {

        mark1 = new Mark("Super", 5);
        mark2 = new Mark("Raczej średnio", 4);
        mark3 = new Mark("Słabo ", 3);

        heniek = new Employee("Heniek", "Zwiewka", "Heniek", "Zwiewka","123997973", null, Arrays.asList(mark1, mark2, mark3));

        employeeService.createEmployee(heniek);

    }


    @Test
    public void shouldGetEmployee() throws Exception {

        //@formatter:off

        given()
                .port(port)
                .auth().preemptive().basic("admin","superAdmin")
                .pathParam("id",heniek.getId())
        .when()
                .get("/employee/{id}")
        .then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .body("name", is(heniek.getName()));


        //@formatter:on
    }

    @Test
    public void shouldGetEmployeeWithAverageOf4() throws Exception {

        //@formatter:off

        given()
                .port(port)
                .auth().preemptive().basic("admin","superAdmin")
                .pathParam("id",heniek.getId())
        .when()
                .get("/employee/{id}")
        .then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .body("scoreAverage", is(4.0));


        //@formatter:on
    }

}