package com.maciek.CleanApp.repositories;

import com.maciek.CleanApp.model.Apartment;
import com.maciek.CleanApp.model.Appointment;
import com.maciek.CleanApp.model.Calendar;
import com.maciek.CleanApp.model.Employer;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@DataJpaTest
@RunWith(SpringRunner.class)
public class ApartmentRepositoryTest {


    @Autowired
    private ApartmentRepository apartmentRepository;

    @Autowired
    private CalendarRepository calendarRepository;

    @Autowired
    private AppointmentRepository appointmentRepository;

    @Autowired
    private EmployerRepository employerRepository;


    private Calendar calendar;
    private Employer augiasz;
    private Apartment stajnia;
    private Appointment appointment;


    @Before
    public void SetUp(){
       appointment = appointmentRepository.save(new Appointment());
        augiasz =  employerRepository.save(new Employer("Augiasz", "Popejasz","Augiasz", "Popejasz","1231231231212", null,null));
        stajnia = new Apartment("Stajnia", 4, "Gnojna", "Athens",augiasz );
        calendar= calendarRepository.save(new Calendar(Arrays.asList(this.appointment)));

        stajnia.setCalendar(calendar);
        apartmentRepository.save(stajnia);
    }

    @Test
    public void ShouldReturnOneApartment(){
        //given setUp
        String athens = "Athens";

        //when
        List<Apartment> query = apartmentRepository.getApartmentsByCityCustomQuery(athens);

        //then

        Assertions.assertThat(query.get(0).getCity()).isEqualTo(athens);
    }

    @Test
    public void findCalendarByApartmentIdCustom() throws Exception {

        //given setup

        //when
        Calendar custom = apartmentRepository.findCalendarByApartmentIdCustom(stajnia.getId());

        //then
        Assertions.assertThat(custom).isNotNull();
        Assertions.assertThat(custom.getAppointments().get(0)).isEqualTo(appointment);


    }

}