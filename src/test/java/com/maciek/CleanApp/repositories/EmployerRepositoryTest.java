package com.maciek.CleanApp.repositories;

import com.maciek.CleanApp.model.Apartment;
import com.maciek.CleanApp.model.Employer;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;


@DataJpaTest
@RunWith(SpringRunner.class)
public class EmployerRepositoryTest {
    @Test
    public void getAllByMarkType() throws Exception {
    }

    @Test
    public void getAllBySurname() throws Exception {
    }


    @Autowired
    private EmployerRepository employerRepository;

    @Test
    public void ShouldCreateNewEmployerWithApartment() throws Exception {

        Apartment apartment = new Apartment("Kamienna", 3, "Niepodleglosci", "Warsaw", null);
        Employer employer = new Employer("Zenek", "Niezgoda", "Zenek", "Niezgoda","123456789", Arrays.asList(apartment), null);


        Employer save = employerRepository.save(employer);



        Assertions.assertThat(save.getId()).isNotNull();

    }
}