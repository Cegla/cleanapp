package com.maciek.CleanApp.repositories;

import com.maciek.CleanApp.model.Employee;
import com.maciek.CleanApp.model.Mark;
import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;


@DataJpaTest
@RunWith(SpringRunner.class)
public class EmployeeRepositoryTest {



    @Autowired
    private EmployeeRepository employeeRepository;


    private Integer score;
    private Employee heniek;
    private Mark mark;

    @Before
    public void SetUp(){
        score = 1;
        mark = new Mark("Slabo sprzata", 1);

        heniek = new Employee("Heniek", "Zwiewka","Heniek", "Zwiewka", "123997973", null, Arrays.asList(mark));
     employeeRepository.save(heniek);
    }


    @Test
    public void shouldFindOneEmployee() throws Exception {
         //given

        //when
        List<Employee> employee = employeeRepository.findByScoreHigherThanCustomQuery(score);

        //then
        Assertions.assertThat(employee).isNotEmpty();
        Assertions.assertThat(employee.get(0).getMarks().get(0).getScore()).isEqualTo(score);

    }







    @Test
    public void shouldReturnOneEmployee() {
        //given


        String heniekName = "Heniek";
        //when
        List<Employee> employees = employeeRepository.findByNameCustomQuery(heniekName);

        //then
        Assertions.assertThat(employees).isNotEmpty();
        Assertions.assertThat(employees.get(0).getMarks().get(0).getScore()).isEqualTo(score);

    }

    @Test
    public void ShouldGetById(){

        //given

        //when
        Employee employee = employeeRepository.findOne(heniek.getId());

        //then
        Assertions.assertThat(employee).isNotNull();
        Assertions.assertThat(employee.getName()).isEqualTo(heniek.getName());
    }

}