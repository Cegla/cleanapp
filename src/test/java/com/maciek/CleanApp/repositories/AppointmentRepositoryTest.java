package com.maciek.CleanApp.repositories;

import com.maciek.CleanApp.model.Appointment;
import com.maciek.CleanApp.model.AppointmentStatus;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


@DataJpaTest
@RunWith(SpringRunner.class)
public class AppointmentRepositoryTest {

    @Autowired
    private AppointmentRepository appointmentRepository;

    Appointment quickAppiontment;
    AppointmentStatus appointmentStatus ;
    LocalDateTime appointmentStartDate;
    LocalDateTime appointmentEndtDate;
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");


    @Before
    public void setUp(){


        appointmentEndtDate  = LocalDateTime.parse("2017-10-10 11:00", formatter);
        appointmentStartDate = LocalDateTime.parse("2017-10-10 09:00",formatter);
        appointmentStatus = AppointmentStatus.UNSIGNED;
        quickAppiontment = appointmentRepository.save(new Appointment(appointmentStartDate,appointmentEndtDate,12,appointmentStatus));

    }


    @Test
    public void shouldGetByDateCustom() throws Exception {

        //given setup +
        LocalDateTime startDate = LocalDateTime.parse("2017-10-05 09:00",formatter);
        LocalDateTime endDate = LocalDateTime.parse("2017-10-12 12:30",formatter);

        //when
        List<Appointment> appointments = appointmentRepository.getAllByDateGreaterThan(startDate);


        //then
        Assertions.assertThat(appointments).isNotEmpty();
        Assertions.assertThat(appointments.get(0).getStartDate()).isGreaterThan(startDate);

    }

    @Test
    public void shouldReturnOneAppointment(){

        //given
        LocalDateTime startDate = LocalDateTime.parse("2017-10-09 09:00",formatter);

        //when
        List<Appointment> appointments = appointmentRepository.getAllByDateGreaterThan(startDate);

        //then
        Assertions.assertThat(appointments).isNotEmpty();
        Assertions.assertThat(appointments.get(0).getStartDate()).isGreaterThan(startDate);

    }

}