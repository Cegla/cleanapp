package com.maciek.CleanApp.repositories;

import com.maciek.CleanApp.model.Apartment;
import com.maciek.CleanApp.model.Challenge;
import com.maciek.CleanApp.model.Employer;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


@DataJpaTest
@RunWith(SpringRunner.class)
public class ChallengeRepositoryTest {



    @Autowired
    private ChallengeRepository challengeRepository;


    @Autowired
    private EmployerRepository employerRepository;

    @Autowired
    private ApartmentRepository apartmentRepository;


    private Apartment apartment;
    private Challenge stillActive;
    private Challenge notActive;
    private Boolean expired;
    private Boolean active;

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    private Employer augiasz;
    private Apartment stajnia;



    @Before
    public void SetUp(){

        LocalDateTime startDate = LocalDateTime.parse("2017-10-05 09:00",formatter);


        expired = false;
        active = true;


        augiasz = employerRepository.save(new Employer("Augiusz11", "Pompejusz","Augiasz", "Popejasz","1231231231212", null,null));
        stajnia = apartmentRepository.save(apartmentRepository.save(new Apartment("Stajnia", 4, "Gnojna", "Athens",augiasz )));

        stillActive = challengeRepository.save(new Challenge(startDate, expired,stajnia, null));
        notActive = challengeRepository.save(new Challenge(startDate, active, null, null));

    }



    @Test
    public void shouldFindChallengeByEmployerNameCustom() throws Exception {

        //given setup

        //when
        List<Challenge> challengeByEmployerNameCustom = challengeRepository.findChallengeByEmployerNameCustom("Augiasz");
        List<Challenge> challengeByEmployerNameCustom2 = challengeRepository.findChallengeByEmployerNameCustom2(augiasz.getName());

        //then
        Assertions.assertThat(challengeByEmployerNameCustom2).isNotEmpty();
        Assertions.assertThat(challengeByEmployerNameCustom).isNotEmpty();

        Assertions.assertThat(challengeByEmployerNameCustom.get(0).getApartment().getEmployer().getName()).isEqualTo(augiasz.getName());
    }

    @Test
    public void shouldFindChallengeByStreetAndCity() throws Exception {


        //given setup

        //when
        List<Challenge> challengeByStreetAndCity = challengeRepository.findChallengeByStreetAndCity(stajnia.getStreet(), stajnia.getCity());

        //then
        Assertions.assertThat(challengeByStreetAndCity).isNotEmpty();
        Assertions.assertThat(challengeByStreetAndCity.get(0).getApartment().getStreet()).isEqualTo(stajnia.getStreet());
    }

    @Test
    public void shouldFindChallengeByCity() throws Exception {

        //given setUp

        //when
        List<Challenge> challengeByCity = challengeRepository.findChallengeByCity(stajnia.getCity());


        //then
        Assertions.assertThat(challengeByCity).isNotEmpty();
        Assertions.assertThat(challengeByCity.get(0).getApartment().getCity()).isEqualTo(stajnia.getCity());

    }


    @Test
    public void shouldFindAll() throws Exception {


        //given setUp


        //when
        List<Challenge> all = challengeRepository.findAll();


        //then
        Assertions.assertThat(all).isNotEmpty();


    }



    @Test
    public void shouldFindChallengeByEmployerId() throws Exception {

        //given setUp


        //when
        Challenge challenge = challengeRepository.findChallengeByEmployerIdCustom(augiasz.getId());


        //then
        Assertions.assertThat(challenge).isNotNull();

    }



    @Test
    public void getByStatus() throws Exception {

        //given setUp

        //when
        List<Challenge> challenges = challengeRepository.getByStatus(active);

        //then
        Assertions.assertThat(challenges).isNotEmpty();
        Assertions.assertThat(challenges.get(0).getStatus()).isEqualTo(active);
    }

    @Test
    public void getByDateBetween() throws Exception {
    }

}